!function(){
  //
  function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : "";
  }
  //
  isCookieConsentRequiredOnLoad = getCookie("CookieConsent");
  console.log('cookie on load is ' + isCookieConsentRequiredOnLoad);
  isMarketingTrueOnLoad = isCookieConsentRequiredOnLoad.includes('marketing:true');
  console.log('On load marketing is ' + isMarketingTrueOnLoad);
  window.addEventListener('CookiebotOnAccept', function (e)
  {
    if (Cookiebot.changed)
    {
      isCookieConsentStillRequired = getCookie("CookieConsent");
      isMarketingStillTrue = isCookieConsentStillRequired.includes('marketing:true');
      console.log('cookie changed to ' + isCookieConsentStillRequired);
      if (isMarketingStillTrue == true)
      {
        console.log('On change marketing is ' + isMarketingStillTrue);
        document.location.reload();
      };
    }
  });
}();
